PKGNAME != sed -n "s/Package: *\([^ ]*\)/\1/p" DESCRIPTION
PKGVERS != sed -n "s/Version: *\([^ ]*\)/\1/p" DESCRIPTION
PKGSRC != basename `pwd`

default: build install clean

cran: docs build check install

dev: build-dev check-dev install

docs:
	R -q --vanilla -e "roxygen2::roxygenise()"
	R -q --vanilla -e "devtools::document()"

build:
	cd ..;\
	R CMD build $(PKGSRC)

build-dev:
	cd ..;\
	R CMD build --no-build-vignettes $(PKGSRC)

check:
	cd ..;\
	R CMD check --as-cran $(PKGNAME)_$(PKGVERS).tar.gz

check-dev:
	cd ..;\
	R CMD check --no-vignettes $(PKGNAME)_$(PKGVERS).tar.gz

install:
	cd ..;\
	R CMD INSTALL $(PKGNAME)_$(PKGVERS).tar.gz

vignettes-only:
	cd ..;\
	R CMD Sweave --pdf $(PKGSRC)/vignettes/pubprint.Rnw

clean:
	cd ..;\
	rm -rf $(PKGNAME).Rcheck/
