Development notes
=================

Add new commit
--------------

- execute `make cran` and solve all warnings and errors
- make commit

Publishing a release
--------------------

- add release date in NEWS.md
- change date field in description file
- change version field in description file
- change version and date in vignette
- correct version in installation section of README.md
- execute `make cran`
- check package by http://win-builder.r-project.org/
- make commit
- add hg tag to commit
- add download to bitbucket.org
- add update to CRAN

To do before next release
-------------------------

- check toClipboard function on Mac OS and Windows.

Ideas
-----

- style.apa.numeric: use output of names() function as argument for out.value
  (name argument)?
- allow exporting of pprint functions to global environment? Pro: Easier
  access
- how to create formatted text to paste into ms office?
- number smaller than .001 should be printed as "0"? (not "0.00") even if
  option `drop0trailing` is `FALSE`?
- examples in manual: explicit format specifier is confusing?
- add function for manova
- add global option for printing estimates?
- add a new pprint function: type in manually all the values for easy copy and
  printing ...
- add confidence intervals ...
- correct titles of help files
- is a list for internal style functions appropriate? wouldn't it be easier
  with ...?
- Add several global options (and as argument to numeric function):
    - use big.mark?
    - set separator to NULL per default
    - set decimal.mark
    - options for arguments of apa functions? like print.estimates, print.n
      and so on?
    - actually how could it be possible to add all format() arguments to
      global functions and to templates (see below)?
- add something like templates for several options, like different nsmalls,
  etc.
- how to include additional text into output? prefix and suffix?
- add support for effect size of anova
- is it possible to simplify default.bracket with changes in html.bracket?
- add xtable support
